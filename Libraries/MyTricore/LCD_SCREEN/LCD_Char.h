/**
 * \file TLF35584.h
 * \brief
 *
 * \version V0.2
 * \copyright Copyright (c) 2015 Infineon Technologies AG. All rights reserved.
 *
 *
 *                                 IMPORTANT NOTICE
 *
 *
 * Infineon Technologies AG (Infineon) is supplying this file for use
 * exclusively with Infineon's microcontroller products. This file can be freely
 * distributed within development tools that are supporting such microcontroller
 * products.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * INFINEON SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * \defgroup IfxLld_Demo_TLF35584_SrcDoc_Main Demo Source
 * \ingroup IfxLld_Demo_TLF35584_SrcDoc
 * \defgroup IfxLld_Demo_TLF35584_SrcDoc_Main_Interrupt Interrupts
 * \ingroup IfxLld_Demo_TLF35584_SrcDoc_Main
 */

#ifndef LCD_Char_H
#define LCD_Char_H 1

/******************************************************************************/
/*----------------------------------Includes----------------------------------*/
/******************************************************************************/
#include <Cpu/Std/Ifx_Types.h>
#include "Configuration.h"
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>

/******************************************************************************/
/*-----------------------------------Macros-----------------------------------*/
/******************************************************************************/
#define TLF_BUFFER_SIZE 1   /**< \brief Tx/Rx Buffer size */
/******************************************************************************/
/*--------------------------------Enumerations--------------------------------*/
/******************************************************************************/

/******************************************************************************/
/*-----------------------------Data Structures--------------------------------*/
/******************************************************************************/

typedef struct
{
    uint32 spiTxBuffer[TLF_BUFFER_SIZE];                               /**< \brief Qspi Transmit buffer */
    uint32 spiRxBuffer[TLF_BUFFER_SIZE];                               /**< \brief Qspi receive buffer */
} AppQspi_LCD_Char_Buffer;

/** \brief QspiCpu global data */
typedef struct
{
    AppQspi_LCD_Char_Buffer qspiBuffer;                       /**< \brief Qspi buffer */
    struct
    {
        IfxQspi_SpiMaster         spiMaster;            /**< \brief Pointer to spi Master handle */
        IfxQspi_SpiMaster_Channel spiMasterChannel;      /**< \brief Spi Master Channel handle */
    }drivers;
}  App_Qspi_LCD_Char_Cpu;

/******************************************************************************/
/*------------------------------Global variables------------------------------*/
/******************************************************************************/
IFX_EXTERN App_Qspi_LCD_Char_Cpu g_Qspi_LCD_Char_Cpu;

/******************************************************************************/
/*-------------------------Function Prototypes--------------------------------*/
/******************************************************************************/
IFX_EXTERN void LCD_Char_init(void);
IFX_EXTERN void LCD_Char_strobe(void);
IFX_EXTERN uint32 LCD_Char_write(uint32 send_data);
IFX_EXTERN void clear_display(void);
IFX_EXTERN void home_line1(void);
IFX_EXTERN void home_line2(void);
IFX_EXTERN void home_line3(void);
IFX_EXTERN void home_line4(void);
IFX_EXTERN void string2lcd(char *lcd_str);
IFX_EXTERN void char2lcd(char a_char);
IFX_EXTERN char *char2hex(char *str,uint8 *hex,int len);

#endif  // LCD_Char_H
