/*
 * LCD_SCREEN.c
 *
 *  Created on: Sep 30, 2021
 *      Author: weetit
 */


#include "LCD_SCREEN.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here

//Global variable

app_LCD_SCREEN g_LCD_SCREEN;

//Code here

void LCD_init(void)
{
    LCD_Char_init();
    g_LCD_SCREEN.col=0;
    g_LCD_SCREEN.row=0;
    memset(g_LCD_SCREEN.LCD_SCEEN[0],20,LCD_COL);
    memset(g_LCD_SCREEN.LCD_SCEEN[1],20,LCD_COL);
    memset(g_LCD_SCREEN.LCD_SCEEN[2],20,LCD_COL);
    memset(g_LCD_SCREEN.LCD_SCEEN[3],20,LCD_COL);
}

void LCD_print(char *data)
{
    size_t len=strlen(data);
    if(strlen>LCD_COL) len=LCD_COL;
    memset(g_LCD_SCREEN.LCD_SCEEN[g_LCD_SCREEN.row],20,LCD_COL);
    memcpy(g_LCD_SCREEN.LCD_SCEEN[g_LCD_SCREEN.row],data,len);
    g_LCD_SCREEN.row++;
    g_LCD_SCREEN.row%=LCD_ROW;
}


void LCD_draw(void)
{
    home_line1();
    string2lcd(g_LCD_SCREEN.LCD_SCEEN[g_LCD_SCREEN.row]);
    g_LCD_SCREEN.row++;
    g_LCD_SCREEN.row%=LCD_ROW;
    home_line2();
    string2lcd(g_LCD_SCREEN.LCD_SCEEN[g_LCD_SCREEN.row]);
    g_LCD_SCREEN.row++;
    g_LCD_SCREEN.row%=LCD_ROW;
    home_line3();
    string2lcd(g_LCD_SCREEN.LCD_SCEEN[g_LCD_SCREEN.row]);
    g_LCD_SCREEN.row++;
    g_LCD_SCREEN.row%=LCD_ROW;
    home_line4();
    string2lcd(g_LCD_SCREEN.LCD_SCEEN[g_LCD_SCREEN.row]);
    g_LCD_SCREEN.row++;
    g_LCD_SCREEN.row%=LCD_ROW;
}
