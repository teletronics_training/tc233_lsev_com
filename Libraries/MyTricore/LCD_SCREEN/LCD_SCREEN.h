/*
 * LCD_SCREEN.h
 *
 *  Created on: Sep 30, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_LCD_SCREEN_LCD_SCREEN_H_
#define LIBRARIES_MYTRICORE_LCD_SCREEN_LCD_SCREEN_H_

#include "LCD_Char.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here

#define LCD_ROW 4
#define LCD_COL 20

//Add ENUM here


//Add Struct here

typedef struct
{
        char LCD_SCEEN[LCD_ROW][LCD_COL+1];
        uint8 row,col;
}app_LCD_SCREEN;

//Add export variable here

IFX_EXTERN app_LCD_SCREEN g_LCD_SCREEN;

//Add function prototype here

IFX_EXTERN void LCD_init(void);
IFX_EXTERN void LCD_print(char *data);
IFX_EXTERN void LCD_draw(void);

#endif /* LIBRARIES_MYTRICORE_LCD_SCREEN_LCD_SCREEN_H_ */
