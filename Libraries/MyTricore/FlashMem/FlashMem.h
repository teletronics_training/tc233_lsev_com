/*
 * FlashMem.h
 *
 *  Created on: Sep 23, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_FLASHMEM_H_
#define LIBRARIES_MYTRICORE_FLASHMEM_H_

#include <Cpu/Std/Ifx_Types.h>

#include "Configuration.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here


//Add ENUM here


//Add Struct here



/** \brief QspiCpu global data */


//Add function prototype here
IFX_EXTERN App_Qspi_Cpu g_Qspi_Cpu;

IFX_EXTERN void Flash_init(void);


#endif /* LIBRARIES_MYTRICORE_FLASHMEM_H_ */
