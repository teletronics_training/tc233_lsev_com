/*
 * POT_SPI.c
 *
 *  Created on: Oct 1, 2021
 *      Author: weetit
 */


#include "POT_SPI.h"
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here

//Global variable

//Code here

void POT_SPI_init(void)
{
    boolean interruptState = IfxCpu_disableInterrupts();
    // create channel config
    IfxQspi_SpiMaster_ChannelConfig spiMasterChannelConfig;
    IfxQspi_SpiMaster_initChannelConfig(&spiMasterChannelConfig, &g_Qspi_Cpu.drivers.spiMaster);

    // set the baudrate for this channel
    spiMasterChannelConfig.base.baudrate = 5000000;
    spiMasterChannelConfig.base.mode.csTrailDelay = 2;
    spiMasterChannelConfig.base.mode.csInactiveDelay = 2;
    spiMasterChannelConfig.base.mode.shiftClock = SpiIf_ShiftClock_shiftTransmitDataOnTrailingEdge;
    spiMasterChannelConfig.base.mode.dataWidth=32;

    // select pin configuration
    const IfxQspi_SpiMaster_Output slsOutput = {
        &POT_ENABLE,
        IfxPort_OutputMode_pushPull,
        IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    spiMasterChannelConfig.sls.output = slsOutput;

    // initialize channel
    IfxQspi_SpiMaster_initChannel(&g_Qspi_Cpu.drivers.spiMasterPOTChannel, &spiMasterChannelConfig);
    IfxCpu_restoreInterrupts(interruptState);

    IfxPort_setPinMode(POT_NRS, IfxPort_Mode_outputPushPullGeneral);
    IfxPort_setPinHigh(POT_NRS);
}

#define POT_WRITE       (1)
#define POT_SEL1        (1)
#define POT_SEL2        (2)
#define POT_IC1_CMD     28
#define POT_IC2_CMD     12
#define POT_IC1_SEL     24
#define POT_IC2_SEL     8
#define POT_IC1_VAL     16
#define POT_IC2_VAL     0


void POT_SPI_write(uint8 POT1, uint8 POT2, uint8 POT3, uint8 POT4)
{
    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterPOTChannel) == SpiIf_Status_busy)  {};
    memset(g_Qspi_Cpu.qspiBuffer.spiTxBuffer,0,TLF_BUFFER_SIZE);

    g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0]=(POT_WRITE<<POT_IC1_CMD)|(POT_SEL1<<POT_IC1_SEL)|(POT1<<POT_IC1_VAL)
                                        |(POT_WRITE<<POT_IC2_CMD)|(POT_SEL1<<POT_IC2_CMD)|(POT3<<POT_IC2_VAL);

    IfxQspi_SpiMaster_exchange(&g_Qspi_Cpu.drivers.spiMasterPOTChannel, &g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0],
            &g_Qspi_Cpu.qspiBuffer.spiRxBuffer[0], 1);

    while (IfxQspi_SpiMaster_getStatus(&g_Qspi_Cpu.drivers.spiMasterPOTChannel) == SpiIf_Status_busy)  {};

    memset(g_Qspi_Cpu.qspiBuffer.spiTxBuffer,0,TLF_BUFFER_SIZE);

    g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0]=(POT_WRITE<<POT_IC1_CMD)|(POT_SEL2<<POT_IC1_SEL)|(POT2<<POT_IC1_VAL)
                                        |(POT_WRITE<<POT_IC2_CMD)|(POT_SEL2<<POT_IC2_CMD)|(POT4<<POT_IC2_VAL);

    IfxQspi_SpiMaster_exchange(&g_Qspi_Cpu.drivers.spiMasterPOTChannel, &g_Qspi_Cpu.qspiBuffer.spiTxBuffer[0],
            &g_Qspi_Cpu.qspiBuffer.spiRxBuffer[0], 1);

}

void POT_Set(float R1,float R2,float R3,float R4)
{
    uint8 POT1, POT2, POT3, POT4;
    uint16 Rw=52;
    if(R1>8330.0) R1=8330.0;
    if(R2>8330.0) R2=8330.0;
    if(R3>8330.0) R3=8330.0;
    if(R4>8330.0) R4=8330.0;

    if(R1<88.0) R1=88.0;
    if(R2<88.0) R2=88.0;
    if(R3<88.0) R3=88.0;
    if(R4<88.0) R4=88.0;


    POT1=(R1-88.0)/(8330.0-88.0)*255;
    POT2=(R2-88.0)/(8330.0-88.0)*255;
    POT3=(R3-88.0)/(8330.0-88.0)*255;
    POT4=(R4-88.0)/(8330.0-88.0)*255;

    POT_SPI_write(POT1, POT2, POT3, POT4);
}

void POT_Volt_Set(float V1,float V2,float V3,float V4)
{
    float R1,R2,R3,R4;
    R1=V1*8800.0/5.0;
    R2=V2*8800.0/5.0;
    R3=V3*8800.0/5.0;
    R4=V4*8800.0/5.0;

    POT_Set(R1,R2,R3,R4);
}
