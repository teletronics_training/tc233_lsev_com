/*
 * PWM_GPIO.h
 *
 *  Created on: Oct 2, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_ECU_PWM_GPIO_H_
#define LIBRARIES_MYTRICORE_ECU_PWM_GPIO_H_

#include <Cpu/Std/Ifx_Types.h>
#include "IfxGtm_Tom_Pwm.h"
#include "ConfigurationECU.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here

//Add ENUM here

typedef enum
{
    PWM_GPIO_0 = 0,
    PWM_GPIO_1,
    PWM_GPIO_2,
    PWM_GPIO_3,
    PWM_GPIO_4,
    PWM_GPIO_Num
} App_PWM_GPIO_Port;


//Add Struct here

typedef struct{
        IfxGtm_Tom_ToutMap *PWM_GPIO;
        uint32 dutyCycle;
        IfxGtm_Tom_Pwm_Config tomConfig;                                  /* Timer configuration structure                */
        IfxGtm_Tom_Pwm_Driver tomDriver;                                  /* Timer Driver structure                       */
}App_PWM_GPIO;

//Add export variable here

IFX_EXTERN App_PWM_GPIO g_PWM_GPIO[PWM_GPIO_Num];

//Add function prototype here

IFX_EXTERN void PWM_GPIO_init(void);
IFX_EXTERN void PWM_GPIO_setDuty(App_PWM_GPIO_Port gpio,uint32 dutyCycle);

#endif /* LIBRARIES_MYTRICORE_ECU_PWM_GPIO_H_ */

