/*
 * GPIO.c
 *
 *  Created on: Oct 2, 2021
 *      Author: weetit
 */


#include "GPIO.h"

/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Include here

//Declaration here

//Global variable

App_GPIO_OutPin g_App_GPIO[GPIO_Num]={
        {
                MCU_IO0,IfxPort_State_low,FALSE
        },
        {
                MCU_IO1,IfxPort_State_low,FALSE
        },
        {
                MCU_IO2,IfxPort_State_low,FALSE
        },
        {
                MCU_IO3,IfxPort_State_low,FALSE
        },
        {
                MCU_IO4,IfxPort_State_low,FALSE
        },
        {
                MCU_IO5,IfxPort_State_low,FALSE
        },
        {
                MCU_IO6,IfxPort_State_low,FALSE
        },
        {
                MCU_IO7,IfxPort_State_low,FALSE
        },
        {
                MCU_IO8,IfxPort_State_low,FALSE
        },
        {
                MCU_IO9,IfxPort_State_low,FALSE
        },
        {
                MCU_IO10,IfxPort_State_low,FALSE
        }
};

//Code here

void GPIO_init(void)
{
    int loop=0;
    for(loop=0;loop<GPIO_Num;loop++)
    {
        if(g_App_GPIO[loop].port!=NULL)
        {
            IfxPort_setPinMode(g_App_GPIO[loop].port, g_App_GPIO[loop].pinIndex, IfxPort_Mode_outputPushPullGeneral);
        }
        GPIO_off(loop);
    }
}

void GPIO_on(App_GPIO_Port gpio)
{
    if(g_App_GPIO[gpio].port!=NULL)
    {
        IfxPort_setPinState(g_App_GPIO[gpio].port, g_App_GPIO[gpio].pinIndex
                , (g_App_GPIO[gpio].activeState == (IfxPort_State_high)? IfxPort_State_high : IfxPort_State_low));
        g_App_GPIO[gpio].status=TRUE;
    }
}

void GPIO_off(App_GPIO_Port gpio)
{
    if(g_App_GPIO[gpio].port!=NULL)
    {
        IfxPort_setPinState(g_App_GPIO[gpio].port, g_App_GPIO[gpio].pinIndex
                , (g_App_GPIO[gpio].activeState == (IfxPort_State_high)? IfxPort_State_low : IfxPort_State_high));
        g_App_GPIO[gpio].status=FALSE;
    }
}
