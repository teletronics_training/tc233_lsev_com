/*
 * POT_SPI.h
 *
 *  Created on: Oct 1, 2021
 *      Author: weetit
 */

#ifndef LIBRARIES_MYTRICORE_ECU_POT_SPI_H_
#define LIBRARIES_MYTRICORE_ECU_POT_SPI_H_

#include <Cpu/Std/Ifx_Types.h>
#include <Qspi/SpiMaster/IfxQspi_SpiMaster.h>
#include "ConfigurationECU.h"


/*
#if defined(AurixBoard_TC265)
#elif defined(AurixBoard_TC233)
#else
#error AurixBoard selection required.
#endif
*/

//Add define here


//Add ENUM here


//Add Struct here


//Add export variable here
IFX_EXTERN App_Qspi_Cpu g_Qspi_Cpu;

//Add function prototype here
IFX_EXTERN void POT_SPI_init(void);
IFX_EXTERN void POT_SPI_write(uint8 POT1, uint8 POT2, uint8 POT3, uint8 POT4);
IFX_EXTERN void POT_Set(float R1,float R2,float R3,float R4);
IFX_EXTERN void POT_Volt_Set(float V1,float V2,float V3,float V4);

#endif /* LIBRARIES_MYTRICORE_ECU_POT_SPI_H_ */

