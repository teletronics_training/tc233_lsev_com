/*
 * MyBSP.h
 *
 *  Created on: Mar 12, 2021
 *      Author: weeti
 */

#ifndef LIBRARIES_MYTRICORE_MYBSP_H_
#define LIBRARIES_MYTRICORE_MYBSP_H_

#include "Ifx_Types.h"
#include "Cpu/Std/IfxCpu.h"
#include "Port/Std/IfxPort.h"
#include "Src/Std/IfxSrc.h"
#include "Stm/Std/IfxStm.h"
#include "Scu/Std/IfxScuCcu.h"

#define TIMER_COUNT       (11)                                      /**< \internal \brief number of timer values defined */
#define TIMER_INDEX_10NS  (0)                                       /**< \internal \brief Index of the time value 10ns*/
#define TIMER_INDEX_100NS (1)                                       /**< \internal \brief Index of the time value 100ns*/
#define TIMER_INDEX_1US   (2)                                       /**< \internal \brief Index of the time value 1us*/
#define TIMER_INDEX_10US  (3)                                       /**< \internal \brief Index of the time value 10us*/
#define TIMER_INDEX_100US (4)                                       /**< \internal \brief Index of the time value 100us*/
#define TIMER_INDEX_1MS   (5)                                       /**< \internal \brief Index of the time value 1ms*/
#define TIMER_INDEX_10MS  (6)                                       /**< \internal \brief Index of the time value 10ms*/
#define TIMER_INDEX_100MS (7)                                       /**< \internal \brief Index of the time value 100ms*/
#define TIMER_INDEX_1S    (8)                                       /**< \internal \brief Index of the time value 1s*/
#define TIMER_INDEX_10S   (9)                                       /**< \internal \brief Index of the time value 10s*/
#define TIMER_INDEX_100S  (10)                                      /**< \internal \brief Index of the time value 100s*/


#define TimeConst_0s    ((Ifx_TickTime)0)                           /**< \brief time constant equal to 1s */
#define TimeConst_10ns  (TimeConst[TIMER_INDEX_10NS])               /**< \brief time constant equal to 10ns */
#define TimeConst_100ns (TimeConst[TIMER_INDEX_100NS])              /**< \brief time constant equal to 100ns */
#define TimeConst_1us   (TimeConst[TIMER_INDEX_1US])                /**< \brief time constant equal to 1us */
#define TimeConst_10us  (TimeConst[TIMER_INDEX_10US])               /**< \brief time constant equal to 10us */
#define TimeConst_100us (TimeConst[TIMER_INDEX_100US])              /**< \brief time constant equal to 100us */
#define TimeConst_1ms   (TimeConst[TIMER_INDEX_1MS])                /**< \brief time constant equal to 1ms */
#define TimeConst_10ms  (TimeConst[TIMER_INDEX_10MS])               /**< \brief time constant equal to 10ms */
#define TimeConst_100ms (TimeConst[TIMER_INDEX_100MS])              /**< \brief time constant equal to 100ms */
#define TimeConst_1s    (TimeConst[TIMER_INDEX_1S])                 /**< \brief time constant equal to 1s */
#define TimeConst_10s   (TimeConst[TIMER_INDEX_10S])                /**< \brief time constant equal to 10s */
#define TimeConst_100s  (TimeConst[TIMER_INDEX_100S])               /**< \brief time constant equal to 100s */

IFX_EXTERN Ifx_TickTime TimeConst[TIMER_COUNT];

#endif /* LIBRARIES_MYTRICORE_MYBSP_H_ */
