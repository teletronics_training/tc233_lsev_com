/*

 * AppTaskFu.c
 *
 *  Created on: 19.09.2013
 *
 */


#include "AppTaskFu.h"
#include "ConfigurationECU.h"
#include "LCD_SCREEN.h"
#include "MULTICANBUS.h"
#include "GTM_TIM_PWM.h"
#include "PWM_GPIO.h"
#include "Relay_GPIO.h"
#include "GPIO.h"
#include "POT_SPI.h"
#include "LSEV_ECU.h"

typedef enum
{
    TEST_BOARD=0,
    TEST_RC,
    TEST_PWM,
    TEST_Relay,
    TEST_GPIO,
    TEST_Number
}Test_Senario_Type;

Test_Senario_Type senario=TEST_RC;


void appTaskfu_1ms(void)
{
//    transmitCanMessage();

}

void appTaskfu_10ms(void)
{

}

void appTaskfu_100ms(void)
{
    char string1[32],string2[32],string3[32],string4[32];
    char string_on[5] ="ON  ";
    char string_off[5]="OFF ";
    char string_NA[5]="N/A ";
    float32 stick_Throttle;
    float32 stick_Turn;
    float32 stick_Brake;
    float32 stick_Ign;
    float32 stick_Dir;
//    sendCanMessage(0x12345678,0xABCDEF00);
    getDutyCycle();

    static uint16 run_once=0;

    static int dead_man_switch=0;

        {
            boolean RC_SIGNAL_CONTROL=FALSE;
            /* Initialize the VADC */
//            sprintf(string1,"Input capture V2....");
            sprintf(string1,"                    ");
            sprintf(string2,"                    ");
            sprintf(string3,"                    ");
            sprintf(string4,"                    ");
//
//
//            ftoa(g_GtmTimPwm[0].g_measuredPwmDutyCycle, &string1[0], 2);
//            ftoa(g_GtmTimPwm[1].g_measuredPwmDutyCycle, &string1[6], 2);
//            ftoa(g_GtmTimPwm[2].g_measuredPwmDutyCycle, &string1[12], 2);
//            ftoa(g_GtmTimPwm[3].g_measuredPwmDutyCycle, &string2[0], 2);
//            ftoa(g_GtmTimPwm[4].g_measuredPwmDutyCycle, &string2[6], 2);
//            ftoa(g_GtmTimPwm[5].g_measuredPwmDutyCycle, &string2[12], 2);

            if((g_GtmTimPwm[0].g_measuredPwmDutyCycle<12)
            &&(g_GtmTimPwm[1].g_measuredPwmDutyCycle<12)
            &&(g_GtmTimPwm[2].g_measuredPwmDutyCycle<12)
            &&(g_GtmTimPwm[3].g_measuredPwmDutyCycle<12)
            &&(g_GtmTimPwm[4].g_measuredPwmDutyCycle<12)
            &&(g_GtmTimPwm[5].g_measuredPwmDutyCycle<12))
            {
                stick_Throttle=g_GtmTimPwm[1].g_measuredPwmDutyCycle;
                stick_Turn=g_GtmTimPwm[0].g_measuredPwmDutyCycle;
                stick_Brake=g_GtmTimPwm[2].g_measuredPwmDutyCycle;
                stick_Ign=g_GtmTimPwm[4].g_measuredPwmDutyCycle;
                stick_Dir=g_GtmTimPwm[5].g_measuredPwmDutyCycle;

                ftoa(stick_Throttle, &string1[0], 2);
                ftoa(stick_Turn, &string1[6], 2);
                ftoa(stick_Brake, &string1[12], 2);
                ftoa(stick_Ign, &string2[0], 2);
                ftoa(stick_Dir, &string2[6], 2);

//Throttle Control
//                static old_throttle=0;
                float throttle,volt;
                if(stick_Throttle>7.7)
                {
                    throttle=stick_Throttle;
//                    float throttle;
//                    RC_SIGNAL_CONTROL=TRUE;
//                    throttle=stick_Throttle-3.5;
//                    if(throttle>9)
//                    {
//                        throttle=9;
//                    }

//                    POT_Volt_Set(2.5,2.5,2.5,2.5);
                }
                else
                {
                    throttle=7.7;
//                    POT_Volt_Set(2.5,2.5,2.5,2.5);
                }
                if(stick_Throttle>9)
                {
                    throttle=9;
                }
                volt=(throttle-7.7)/(9-7.7)*3+1;
                POT_Volt_Set(volt,volt,volt,volt);

//Ignition Control
                static boolean ignition=FALSE;
                static boolean direction=FALSE;
                if(stick_Ign<7)
                {
                    Relay_GPIO_off(LSEV_IGNITION);
                    if(ignition)
                    {
                        RC_SIGNAL_CONTROL=TRUE;
                    }
                    ignition=FALSE;
                }
                else if(stick_Ign>8)
                {
                    Relay_GPIO_on(LSEV_IGNITION);
                    if(!ignition)
                    {
                        RC_SIGNAL_CONTROL=TRUE;
                    }
                    ignition=TRUE;
                }

//Direction Control
                if(stick_Dir<7)
                {
                    Relay_GPIO_off(LSEV_DIRECTION);
                    if(direction)
                    {
                        RC_SIGNAL_CONTROL=TRUE;
                    }
                    direction=FALSE;
                }
                else if(stick_Dir>8)
                {
                    Relay_GPIO_on(LSEV_DIRECTION);
                    if(!direction)
                    {
                        RC_SIGNAL_CONTROL=TRUE;
                    }
                    direction=TRUE;
                }

//Brake Control
                if((stick_Brake<9) && (stick_Brake>6))
                {
                    GPIO_off(LSEV_BRAKE_INC);
                    GPIO_off(LSEV_BRAKE_DEC);
                    GPIO_off(LSEV_BRAKE_EN);
                    PWM_GPIO_setDuty(LSEV_BRAKE_PWM,100);
                }
                else
                {
                    if(stick_Brake<=6)
                    {
                        GPIO_on(LSEV_BRAKE_INC);
                        GPIO_off(LSEV_BRAKE_DEC);
                        GPIO_on(LSEV_BRAKE_EN);
                        PWM_GPIO_setDuty(LSEV_BRAKE_PWM,100);
                        RC_SIGNAL_CONTROL=TRUE;
                    }
                    else
                    {
                        GPIO_off(LSEV_BRAKE_INC);
                        GPIO_on(LSEV_BRAKE_DEC);
                        GPIO_on(LSEV_BRAKE_EN);
                        PWM_GPIO_setDuty(LSEV_BRAKE_PWM,100);
                        RC_SIGNAL_CONTROL=TRUE;
                    }
                }

//Steering Control
                if(stick_Turn<7)
                {
                    GPIO_on(LSEV_STREERING_RT);
                    GPIO_off(LSEV_STREERING_LT);
                    Relay_GPIO_on(LSEV_STREERING_RT_RELAY);
                    Relay_GPIO_off(LSEV_STREERING_LT_RELAY);
                    PWM_GPIO_setDuty(LSEV_STREERING_PWM,70);
                    RC_SIGNAL_CONTROL=TRUE;
                }
                else if(stick_Turn>8)
                {
                    GPIO_off(LSEV_STREERING_RT);
                    GPIO_on(LSEV_STREERING_LT);
                    Relay_GPIO_off(LSEV_STREERING_RT_RELAY);
                    Relay_GPIO_on(LSEV_STREERING_LT_RELAY);
                    PWM_GPIO_setDuty(LSEV_STREERING_PWM,70);
                    RC_SIGNAL_CONTROL=TRUE;
                }
                else
                {
                    GPIO_on(LSEV_STREERING_RT);
                    GPIO_on(LSEV_STREERING_LT);
                    Relay_GPIO_off(LSEV_STREERING_RT_RELAY);
                    Relay_GPIO_off(LSEV_STREERING_LT_RELAY);
                    PWM_GPIO_setDuty(LSEV_STREERING_PWM,0);
                }

//                ftoa(g_GtmTimPwm[0].g_measuredPwmPeriod, &string4[0], 4);
//                if(g_GtmTimPwm[0].g_dataCoherent)
//                {
//                    string4[7]='T';
//                }
//                else
//                {
//                    string4[7]='F';
//                }
            }
//            if(RC_SIGNAL_CONTROL)
//            {
//                Relay_GPIO_on(LSEV_BUZZER_RELAY);
//            }
//            else
//            {
//                Relay_GPIO_off(LSEV_BUZZER_RELAY);
//            }
        }

    LCD_print(string1);
    LCD_print(string2);
    LCD_print(string3);
    LCD_print(string4);
}

void appTaskfu_1000ms(void)
{
    LCD_draw();
}

