################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/MyTricore/LCD_SCREEN/LCD_Char.c \
../Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.c 

COMPILED_SRCS += \
./Libraries/MyTricore/LCD_SCREEN/LCD_Char.src \
./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.src 

C_DEPS += \
./Libraries/MyTricore/LCD_SCREEN/LCD_Char.d \
./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.d 

OBJS += \
./Libraries/MyTricore/LCD_SCREEN/LCD_Char.o \
./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.o 


# Each subdirectory must supply rules for building sources it contributes
Libraries/MyTricore/LCD_SCREEN/%.src: ../Libraries/MyTricore/LCD_SCREEN/%.c Libraries/MyTricore/LCD_SCREEN/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING C/C++ Compiler'
	cctc -cs --dep-file="$(basename $@).d" --misrac-version=2004 -D__CPU__=tc23x -DAurixBoard_TC233 "-fC:/Users/Dobermann/AURIX-v1.9.20-workspace/TC233_LSEV_COM/TriCore Debug (TASKING)/TASKING_C_C___Compiler-Include_paths__-I_.opt" --iso=99 --c++14 --language=+volatile --exceptions --anachronisms --fp-model=3 -O0 --tradeoff=4 --compact-max-size=200 -g -Wc-w544 -Wc-w557 -Ctc23x -Y0 -N0 -Z0 -o "$@" "$<" && \
	if [ -f "$(basename $@).d" ]; then sed.exe -r  -e 's/\b(.+\.o)\b/Libraries\/MyTricore\/LCD_SCREEN\/\1/g' -e 's/\\/\//g' -e 's/\/\//\//g' -e 's/"//g' -e 's/([a-zA-Z]:\/)/\L\1/g' -e 's/\d32:/@TARGET_DELIMITER@/g; s/\\\d32/@ESCAPED_SPACE@/g; s/\d32/\\\d32/g; s/@ESCAPED_SPACE@/\\\d32/g; s/@TARGET_DELIMITER@/\d32:/g' "$(basename $@).d" > "$(basename $@).d_sed" && cp "$(basename $@).d_sed" "$(basename $@).d" && rm -f "$(basename $@).d_sed" 2>/dev/null; else echo 'No dependency file to process';fi
	@echo 'Finished building: $<'
	@echo ' '

Libraries/MyTricore/LCD_SCREEN/LCD_Char.o: ./Libraries/MyTricore/LCD_SCREEN/LCD_Char.src Libraries/MyTricore/LCD_SCREEN/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.o: ./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.src Libraries/MyTricore/LCD_SCREEN/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-Libraries-2f-MyTricore-2f-LCD_SCREEN

clean-Libraries-2f-MyTricore-2f-LCD_SCREEN:
	-$(RM) ./Libraries/MyTricore/LCD_SCREEN/LCD_Char.d ./Libraries/MyTricore/LCD_SCREEN/LCD_Char.o ./Libraries/MyTricore/LCD_SCREEN/LCD_Char.src ./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.d ./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.o ./Libraries/MyTricore/LCD_SCREEN/LCD_SCREEN.src

.PHONY: clean-Libraries-2f-MyTricore-2f-LCD_SCREEN

