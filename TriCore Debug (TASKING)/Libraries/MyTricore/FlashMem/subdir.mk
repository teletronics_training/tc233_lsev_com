################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Libraries/MyTricore/FlashMem/FlashCmd.c \
../Libraries/MyTricore/FlashMem/FlashDevice.c \
../Libraries/MyTricore/FlashMem/FlashMem.c 

COMPILED_SRCS += \
./Libraries/MyTricore/FlashMem/FlashCmd.src \
./Libraries/MyTricore/FlashMem/FlashDevice.src \
./Libraries/MyTricore/FlashMem/FlashMem.src 

C_DEPS += \
./Libraries/MyTricore/FlashMem/FlashCmd.d \
./Libraries/MyTricore/FlashMem/FlashDevice.d \
./Libraries/MyTricore/FlashMem/FlashMem.d 

OBJS += \
./Libraries/MyTricore/FlashMem/FlashCmd.o \
./Libraries/MyTricore/FlashMem/FlashDevice.o \
./Libraries/MyTricore/FlashMem/FlashMem.o 


# Each subdirectory must supply rules for building sources it contributes
Libraries/MyTricore/FlashMem/%.src: ../Libraries/MyTricore/FlashMem/%.c Libraries/MyTricore/FlashMem/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING C/C++ Compiler'
	cctc -cs --dep-file="$(basename $@).d" --misrac-version=2004 -D__CPU__=tc23x -DAurixBoard_TC233 "-fC:/Users/Dobermann/AURIX-v1.9.20-workspace/TC233_LSEV_COM/TriCore Debug (TASKING)/TASKING_C_C___Compiler-Include_paths__-I_.opt" --iso=99 --c++14 --language=+volatile --exceptions --anachronisms --fp-model=3 -O0 --tradeoff=4 --compact-max-size=200 -g -Wc-w544 -Wc-w557 -Ctc23x -Y0 -N0 -Z0 -o "$@" "$<" && \
	if [ -f "$(basename $@).d" ]; then sed.exe -r  -e 's/\b(.+\.o)\b/Libraries\/MyTricore\/FlashMem\/\1/g' -e 's/\\/\//g' -e 's/\/\//\//g' -e 's/"//g' -e 's/([a-zA-Z]:\/)/\L\1/g' -e 's/\d32:/@TARGET_DELIMITER@/g; s/\\\d32/@ESCAPED_SPACE@/g; s/\d32/\\\d32/g; s/@ESCAPED_SPACE@/\\\d32/g; s/@TARGET_DELIMITER@/\d32:/g' "$(basename $@).d" > "$(basename $@).d_sed" && cp "$(basename $@).d_sed" "$(basename $@).d" && rm -f "$(basename $@).d_sed" 2>/dev/null; else echo 'No dependency file to process';fi
	@echo 'Finished building: $<'
	@echo ' '

Libraries/MyTricore/FlashMem/FlashCmd.o: ./Libraries/MyTricore/FlashMem/FlashCmd.src Libraries/MyTricore/FlashMem/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Libraries/MyTricore/FlashMem/FlashDevice.o: ./Libraries/MyTricore/FlashMem/FlashDevice.src Libraries/MyTricore/FlashMem/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Libraries/MyTricore/FlashMem/FlashMem.o: ./Libraries/MyTricore/FlashMem/FlashMem.src Libraries/MyTricore/FlashMem/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: TASKING Assembler'
	astc -Og -Os --no-warnings= --error-limit=42 -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-Libraries-2f-MyTricore-2f-FlashMem

clean-Libraries-2f-MyTricore-2f-FlashMem:
	-$(RM) ./Libraries/MyTricore/FlashMem/FlashCmd.d ./Libraries/MyTricore/FlashMem/FlashCmd.o ./Libraries/MyTricore/FlashMem/FlashCmd.src ./Libraries/MyTricore/FlashMem/FlashDevice.d ./Libraries/MyTricore/FlashMem/FlashDevice.o ./Libraries/MyTricore/FlashMem/FlashDevice.src ./Libraries/MyTricore/FlashMem/FlashMem.d ./Libraries/MyTricore/FlashMem/FlashMem.o ./Libraries/MyTricore/FlashMem/FlashMem.src

.PHONY: clean-Libraries-2f-MyTricore-2f-FlashMem

